# ====================================================================================================
# Simple Mult-File C Project
# Written by Andrew E. Toy <caianda@gmail.com>
# ====================================================================================================

all: Makefile main.o counter.o
	gcc -o counter main.o counter.o

main.o: main.c counter.h
	gcc -c main.c

counter.o: counter.c counter.h
	gcc -c counter.c

clean:
	rm counter *.o
