Simple Multi-File C Project
===========================

Objective
---------

This project shows how to create a simple multi-file C project. The functionality
is defined in the counter files. The main simply calls the functions in counter.

Project Details
---------------

|            |                                                                                                      |
| ---------- | ---------------------------------------------------------------------------------------------------- |
| Author     | Andrew Toy                                                                                           |
| Email      | caianda@gmail.com                                                                                    |
| Repository | [https://bitbucket.org/caianda/multic/src/master/](https://bitbucket.org/caianda/multic/src/master/) |

Compile and Build
-----------------

To build the project:

    % make

This will generate object files for main and counter. The last step in the build
process will "link" the main.o and counter.o into an executable file (counter).

To run the program:

    % ./counter

The program will output:

    Hello, World!
    count = 0
    count = 1
    count = 2
