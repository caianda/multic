// ====================================================================================================
// Simple Mult-File C Project
// Written by Andrew E. Toy <caianda@gmail.com>
// ====================================================================================================

#include <stdio.h>
#include "counter.h"

int count;

void greetings(void) {
  printf("Hello, World!\n");
}

void init(void) {
  count = 0;
}

void displayCount(void) {
  printf("count = %d\n", count);
}

void incCount(void) {
  count++;
}
