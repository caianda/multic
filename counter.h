// ====================================================================================================
// Simple Mult-File C Project
// Written by Andrew E. Toy <caianda@gmail.com>
// ====================================================================================================

#ifndef __COUNTER_H__
#define __COUNTER_H__

void greetings(void);
void init(void);
void displayCount(void);
void incCount(void);

#endif
