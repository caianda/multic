// ====================================================================================================
// Simple Mult-File C Project
// Written by Andrew E. Toy <caianda@gmail.com>
// ====================================================================================================

#include "counter.h"

int main(void) {
  greetings();
  init();
  displayCount();
  incCount();
  displayCount();
  incCount();
  displayCount();
}
